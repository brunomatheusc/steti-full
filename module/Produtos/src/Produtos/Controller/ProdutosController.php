<?php

namespace Produtos\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;

class ProdutosController extends AbstractActionController{
    public function indexAction(){
        $view = new ViewModel();
        $view->setTemplate('produtos/index/index');
        
        return $view;
    }
    
    public function kuhnAction(){
        $view = new ViewModel();
        $view->setTemplate('produtos/kuhn/index');
        
        return $view;        
    }
    
    public function multimarcasAction(){
        $view = new ViewModel();
        $view->setTemplate('produtos/index/index');
        
        return $view;                
    }
    
    public function cadastrarAction(){
        $view = new ViewModel();
        $view->setTemplate("produtos/index/cadastro");
        
        return $view;
    }
}