<?php

namespace Login\Controller;

use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;

class LoginController extends AbstractActionController{
    public function indexAction(){
        $view = new ViewModel();
        $view->setTemplate('login/login');
        
        return $view;
    }
    
    public function acessarAction(){
        $request = $this->getRequest();
        $sessao = new Container('us_usuario');
        
        if ($request->isPost()){
            $params = $this->params()->fromPost();
            
            $user = $params['usuario'];
            $senha = $params['senha'];
            
            if ($user == "admin" && $senha == 9999){
                $sessao->logado = true;
                $sessao->tipo = 1;
                $retorno = array("retorno" => true, "msg" => "Sucesso");
            } else {
                $retorno = array("retorno" => false, "msg" => utf8_encode("Usuário ou senha incorretos"));                
            }
        }
        
        return new JsonModel($retorno);
    }
}
