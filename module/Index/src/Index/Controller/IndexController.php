<?php

namespace Index\Controller;

use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController{
    public function indexAction(){
        $sessao = new Container('us_usuario');
        
        if ($sessao->logado && $sessao->tipo == 1){
            $this->redirect()->toUrl('/escritorio');            
        } else {
            $view = new ViewModel();
            $view->setTemplate('index/index');
        }
        
        return $view;
    }
}
