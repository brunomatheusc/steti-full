<?php

namespace Loja\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class LojaController extends AbstractActionController{
    public function indexAction(){
        $view = new ViewModel();
        $view->setTemplate('loja/index/index');
        
        return $view;
    }
}