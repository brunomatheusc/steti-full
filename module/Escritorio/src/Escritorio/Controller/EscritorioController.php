<?php

namespace Escritorio\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class EscritorioController extends AbstractActionController{
    public function indexAction(){
        $view = new ViewModel();
        $view->setTemplate('escritorio/index/index');
        
        return $view;
    }
}